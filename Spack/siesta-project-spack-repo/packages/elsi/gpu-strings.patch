diff --git a/CMakeLists.txt b/CMakeLists.txt
index 7b2f6230..57dda68c 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -103,6 +103,14 @@ generate_library_targets(LIB_PATHS LIBS)
 ### Compilations ###
 IF(USE_EXTERNAL_ELPA)
   MESSAGE(STATUS "Using external ELPA")
+  IF("${ELSI_ELPA_GPU_STRING}" STREQUAL "")
+    MESSAGE(WARNING "ELSI_ELPA_GPU_STRING not set for external ELPA library")
+    MESSAGE(WARNING "Setting 'gpu' by default")
+    SET(ELSI_ELPA_GPU_STRING "gpu" CACHE STRING "String to be used in the ELPA 'set' method for GPU support")
+    # Alternatively, die here
+    # MESSAGE(FATAL_ERROR "ELSI_ELPA_GPU_STRING not set for external ELPA library")
+    #
+  ENDIF()
 ELSE()
   ADD_SUBDIRECTORY(external/ELPA)
 ENDIF()
diff --git a/external/ELPA/CMakeLists.txt b/external/ELPA/CMakeLists.txt
index 6d9ca216..e03f85eb 100644
--- a/external/ELPA/CMakeLists.txt
+++ b/external/ELPA/CMakeLists.txt
@@ -1,16 +1,21 @@
 IF(USE_ELPA_2020)
     MESSAGE(STATUS "Enabling internal ELPA-2020.05.001")
     MESSAGE(STATUS "WARNING: Internal ELPA may not be as efficient and updated as the external build!")
+    SET(ELSI_ELPA_GPU_STRING "gpu" CACHE STRING "String to be used in the ELPA 'set' method for GPU support")
     ADD_SUBDIRECTORY(ELPA-2020.05.001)
 ELSEIF(USE_ELPA_2021)
     MESSAGE(STATUS "Enabling internal ELPA-2021.11.001")
     MESSAGE(STATUS "WARNING: Internal ELPA may not be as efficient and updated as the external build!")
+    SET(ELSI_ELPA_GPU_STRING "nvidia-gpu" CACHE STRING "String to be used in the ELPA 'set' method for GPU support")
     ADD_SUBDIRECTORY(ELPA-2021.11.001)
 ELSEIF(USE_ELPA_2023)
     MESSAGE(STATUS "You are about to use the internal ELPA-2023 version. Use is still experimental. Please check carefully your results.")
+    SET(ELSI_ELPA_GPU_STRING "nvidia-gpu" CACHE STRING "String to be used in the ELPA 'set' method for GPU support")
     ADD_SUBDIRECTORY(ELPA-2023.05.001)
+    SET(ELSI_ELPA_GPU_STRING "nvidia-gpu" CACHE STRING "String to be used in the ELPA 'set' method for GPU support")
 ELSE()
     MESSAGE(STATUS "Enabling internal ELPA-2020.05.001")
     MESSAGE(STATUS "WARNING: Internal ELPA may not be as efficient and updated as the external build!")
+    SET(ELSI_ELPA_GPU_STRING "gpu" CACHE STRING "String to be used in the ELPA 'set' method for GPU support")
     ADD_SUBDIRECTORY(ELPA-2020.05.001)
 ENDIF()
diff --git a/src/elsi_datatype.f90 b/src/elsi_datatype.f90
index b3785f7d..23f5b0e4 100644
--- a/src/elsi_datatype.f90
+++ b/src/elsi_datatype.f90
@@ -166,6 +166,7 @@ module ELSI_DATATYPE
       integer(kind=i4) :: elpa_n_lcol ! Non-ill-conditioned basis functions
       logical :: elpa_first
       logical :: elpa_started = .false.
+      character(len=30) :: elpa_gpu_string = "gpu"
       class(elpa_t), pointer :: elpa_aux => null()
       class(elpa_t), pointer :: elpa_solve => null()
       class(elpa_autotune_t), pointer :: elpa_tune => null()
diff --git a/src/elsi_elpa.f90 b/src/elsi_elpa.f90
index 1c917960..a0bea016 100644
--- a/src/elsi_elpa.f90
+++ b/src/elsi_elpa.f90
@@ -108,11 +108,17 @@ subroutine elsi_init_elpa(ph,bh)
 
    character(len=*), parameter :: caller = "elsi_init_elpa"
 
+   external :: elsi_get_elpa_gpu_string
+
    if(.not. ph%elpa_started) then
+
       ierr = elpa_init(20180525)
 
       call elsi_check_err(bh,"ELPA initialization failed",ierr,caller)
 
+      call elsi_get_elpa_gpu_string(ph%elpa_gpu_string,ierr)
+      call elsi_check_err(bh,"ELPA GPU string initialization failed",ierr,caller)
+
       call MPI_Comm_split(bh%comm,bh%my_pcol,bh%my_prow,ph%elpa_comm_row,ierr)
 
       call elsi_check_err(bh,"MPI_Comm_split",ierr,caller)
@@ -1715,9 +1721,9 @@ subroutine elsi_elpa_setup(ph,bh,is_aux)
          call ph%elpa_aux%set("solver",2,ierr)
 
          if(ph%elpa_gpu == UNSET .or. ph%elpa_gpu == 0) then
-            call ph%elpa_aux%set("nvidia-gpu",0,ierr)
+            call ph%elpa_aux%set(trim(ph%elpa_gpu_string),0,ierr)
          else
-            call ph%elpa_aux%set("nvidia-gpu",1,ierr)
+            call ph%elpa_aux%set(trim(ph%elpa_gpu_string),1,ierr)
             call ph%elpa_aux%set("real_kernel",ELPA_2STAGE_REAL_GPU,ierr)
             call ph%elpa_aux%set("complex_kernel",ELPA_2STAGE_COMPLEX_GPU,ierr)
          end if
@@ -1725,9 +1731,9 @@ subroutine elsi_elpa_setup(ph,bh,is_aux)
          call ph%elpa_aux%set("solver",1,ierr)
 
          if(ph%elpa_gpu == UNSET .or. ph%elpa_gpu == 0) then
-            call ph%elpa_aux%set("nvidia-gpu",0,ierr)
+            call ph%elpa_aux%set(trim(ph%elpa_gpu_string),0,ierr)
          else
-            call ph%elpa_aux%set("nvidia-gpu",1,ierr)
+            call ph%elpa_aux%set(trim(ph%elpa_gpu_string),1,ierr)
          end if
       end if
    else
@@ -1771,7 +1777,7 @@ subroutine elsi_elpa_setup(ph,bh,is_aux)
       end if
 
       if(ph%elpa_gpu /= UNSET) then
-         call ph%elpa_solve%set("nvidia-gpu",ph%elpa_gpu,ierr)
+         call ph%elpa_solve%set(trim(ph%elpa_gpu_string),ph%elpa_gpu,ierr)
       end if
 
       if(ph%elpa_gpu == 1) then
diff --git a/src/elsi_util.f90 b/src/elsi_util.f90
index 12f1aaca..59d2f58d 100644
--- a/src/elsi_util.f90
+++ b/src/elsi_util.f90
@@ -133,6 +133,7 @@ subroutine elsi_reset_param(ph)
    ph%elpa_n_lcol = UNSET
    ph%elpa_first = .true.
    ph%elpa_started = .false.
+   ph%elpa_gpu_string = "gpu"
    ph%omm_n_lrow = UNSET
    ph%omm_n_elpa = 5
    ph%omm_flavor = 0
diff --git a/src/elsi_version.f90.in b/src/elsi_version.f90.in
index cc20549e..b53ab4a0 100644
--- a/src/elsi_version.f90.in
+++ b/src/elsi_version.f90.in
@@ -102,3 +102,24 @@ subroutine elsi_get_chase_enabled(chase_enabled)
    chase_enabled = @ENABLE_CHASE_BOOL@
 
 end subroutine
+!>
+!! Return the GPU string to be used in the "set" method in ELPA
+!!
+subroutine elsi_get_elpa_gpu_string(gpu_string,status)
+
+   implicit none
+
+   character(len=*), intent(out) :: gpu_string !< GPU string for ELPA
+   integer, intent(out)          :: status
+
+   character(len=*), parameter :: compiled_gpu_string = "@ELSI_ELPA_GPU_STRING@"
+
+   if (len(gpu_string) >= len(compiled_gpu_string)) then
+      gpu_string = compiled_gpu_string
+      status = 0
+   else
+      status = -1
+      gpu_string = "----"
+   endif
+
+end subroutine
