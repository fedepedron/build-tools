# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

# Package updated (wip) by Alberto Garcia for modern versions and CUDA support

class Elsi(CMakePackage, CudaPackage):
    """ELSI provides a unified interface for electronic structure
    codes to a variety of eigenvalue solvers."""

    homepage = "https://wordpress.elsi-interchange.org/"
    git = "https://gitlab.com/elsi_project/elsi_interface.git"
    url = "https://gitlab.com/elsi_project/elsi_interface/-/archive/v2.10.1/elsi_interface-v2.10.1.tar.gz"

    version('2.11.0', tag="v2.11.0")
    # This version contains the (short-lived) ELPA_2024 internal version
    version('2.11-alpha-1', commit="8f23609ccd4c372f83ec")
    version('2.10.1', sha256="b3c7526d46a9139a26680787172a3df15bc648715a35bdf384053231e94ab829")


    # Variants (translation of cmake options)
    variant("add_underscore", default=False, description="Suffix C functions with an underscore")

    variant("cuda", default=False, description="Enable CUDA for GPU acceleration")

    # Note that CPU advanced kernels are not compatible with CUDA support
    # This is an ELSI restriction
    variant(
        "elpa2_kernel",
        default="none",
        description="ELPA2 Kernel",
        values=("none", conditional("AVX", "AVX2", "AVX512",when="~cuda")),
        multi=False,
    )

    variant(
        "elpa_version",
        default="2020",
        description="ELPA built-in version",
        values=("2020", "2021", "2023", "2023_11", "2024"),
        multi=False,
        when="@2.11-alpha-1",
    )

    variant("enable_pexsi", default=False, description="Enable PEXSI support")
    variant("enable_sips", default=False, description="Enable SLEPc-SIPs support")
    variant("use_external_elpa", default=False, description="Build ELPA using SPACK")
    variant("use_external_ntpoly", default=False, description="Build NTPoly using SPACK")

    # Basic dependencies
    depends_on("cmake", type="build")
    depends_on("flex@2.6.4", type="build", when='+enable_pexsi')
    depends_on("bison", type="build", when='+enable_pexsi')

    depends_on("blas", type="link")
    depends_on("lapack", type="link")
    depends_on("mpi")
    depends_on("scalapack", type="link")

    # Library dependencies
    depends_on("elpa@2020.05.001:~openmp", when="+use_external_elpa")
    depends_on("ntpoly", when="+use_external_ntpoly")
    depends_on("slepc", when="+enable_sips")
    depends_on("petsc", when="+enable_sips")

    conflicts("cuda_arch=none", when="+cuda", msg="CUDA architecture is required")

    # Patches to work around problems in released versions
    # For everything below 2.11 (note trick)
    patch("mpi.patch",when="@:2.11-alpha-1")
    patch("ptscotch.patch",when="@:2.11-alpha-1")
    patch("gpu-strings.patch",when="@:2.11-alpha-1")

    # Not yet applied anywhere
    patch("gnuflags.patch")

    def cmake_args(self):
        from os.path import dirname

        spec = self.spec
        args = []

        # Compiler Information
        # (ELSI wants these explicitly set)
        args += ["-DCMAKE_Fortran_COMPILER=" + self.spec["mpi"].mpifc]
        args += ["-DCMAKE_C_COMPILER=" + self.spec["mpi"].mpicc]
        args += ["-DCMAKE_CXX_COMPILER=" + self.spec["mpi"].mpicxx]

        # Handle the various variants
        if "+add_underscore" in self.spec:
            args += ["-DADD_UNDERSCORE=ON"]
        if (
            "elpa2_kernel" in self.spec.variants
            and self.spec.variants["elpa2_kernel"].value != "none"
        ):
            kernel = self.spec.variants["elpa2_kernel"].value
            args += ["-DELPA2_KERNEL=" + kernel]

        # Select built-in ELPA version
        elpa_version = self.spec.variants["elpa_version"].value
        args.append(f"-DUSE_ELPA_{elpa_version}=ON")


        if "+cuda" in self.spec:
             # Set up the cuda macros needed by the build
             args.append("-DUSE_GPU_CUDA=ON")
             # experimental -- we really need the paths and then the library names.
             # Also available: self.spec["cuda"].prefix)

             ###  Note library info:  self.spec["cuda"].libs.link_flags

             cuda = self.spec["cuda"]
             cudart_lib = find(cuda.prefix, "libcudart.so")
             args += ["-DLIB_PATHS=" + dirname(cudart_lib[0])]
             args += ["-DLIBS=cublas;cudart"]
             
             cuda_arch_list = self.spec.variants["cuda_arch"].value
             cuda_arch = cuda_arch_list[0]
             if cuda_arch != "none":
                 args.append(f"-DCMAKE_CUDA_FLAGS=-arch=sm_{cuda_arch} -O3 -std=c++11")
                 args.append(f"-DCMAKE_CUDA_ARCHITECTURES={cuda_arch}")
             
        if "+enable_pexsi" in self.spec:
            args += ["-DENABLE_PEXSI=ON"]

        if "+enable_sips" in self.spec:
            args += ["-DENABLE_SIPS=ON"]

        if "+use_external_elpa" in self.spec:
            args += ["-DUSE_EXTERNAL_ELPA=ON"]
            # Setup the searchpath for elpa
            elpa = self.spec["elpa"]
            elpa_module = find(elpa.prefix, "elpa.mod")
            args += ["-DINC_PATHS=" + dirname(elpa_module[0])]

        if "+use_external_ntpoly" in self.spec:
            args += ["-DUSE_EXTERNAL_NTPOLY=ON"]

        # Only when using fujitsu compiler
        if self.spec.satisfies("%fj"):
            args += ["-DCMAKE_Fortran_MODDIR_FLAG=-M"]

        return args
