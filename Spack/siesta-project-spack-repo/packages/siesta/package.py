from spack import *

class Siesta(CMakePackage,CudaPackage):
    """An efficient DFT simulation code (recipe from Siesta-Project)"""

    homepage = "https://siesta-project.org/siesta"

    git = 'https://gitlab.com/siesta-project/siesta.git'

    # Dictionary to store version data
    _version_data = {

        # This is always "newer than anything"
        'develop': {'branch': 'dev'},
        
        # Spack treats this as newer than any of the 5.2.X
        '5.2-head': {'branch': 'rel-5.2'},

        #'5.0-head': {'branch': 'rel-5.0'},

        # Example of point along a release branch
        # Oct 4, 2024 -- rel-5.0 branch -- After fix for vdw grid
        # '5.0.1-3': {'commit': '8a95f6e04', 'branch': 'rel-5.0'},

        # Released versions
        '5.2.0': {'tag': '5.2.0'},
        '5.0.2': {'tag': '5.0.2'},
        '5.0.1': {'tag': '5.0.1'},
        '5.0.0': {'tag': '5.0.0'},

        # Add more versions as needed
    }

    preferred_version = '5.2.0'
    deprecated_versions = ['5.0.0','5.0.1']

    # Programmatically generate version entries

    for ver, metadata in _version_data.items():
        tag = metadata.get('tag')
        commit = metadata.get('commit')
        branch = metadata.get('branch', None)

        # Use tag if available, else fall back to commit
        if tag:
            version(ver, tag=tag, preferred=(ver == preferred_version),
                         deprecated=(ver in deprecated_versions))
        elif commit:
            version(ver, commit=commit, preferred=(ver == preferred_version),
                         deprecated=(ver in deprecated_versions))
        elif branch:
            version(ver, branch=branch, preferred=(ver == preferred_version),
                         deprecated=(ver in deprecated_versions))


    variant('mpi', default=True, description='Use MPI')
    variant('openmp', default=False, description='Build with OpenMP support (very few utilities will benefit)')
    variant('netcdf', default=False, description='Use NetCDF')
    variant('libxc', default=False, description='Use libxc')
    variant('elpa', default=False, description='Use ELPA library (native interface)')
    variant('fftw', default=True, description='Use FFTW library (needed only for STM/ol-stm)')
    variant('dftd3', default=True,  description='Support for DFT-D3 corrections')
    variant('flook', default=False,  description='Use flook')
    variant('gridsp', default=False,  description='Use single-precision grid operations')
    variant('pexsi', default=False,  description='Use (native) PEXSI interface')
    variant('wannier90', default=False,  description='Use Wannier90 on-the-fly wrapper')

    # ELSI support only available in 5.2.X

    with when("@5.2:"):

        variant("elsi", default=True, description="Use ELSI interface")
        variant("elsi_with_gpu", default=False, description="Enable GPU support in ELSI")
        variant("elsi_with_pexsi", default=True, description="Enable PEXSI support in ELSI")

        # These versions compile ELSI on the fly, so no explicit dependency on "elsi"
        depends_on("bison", when="+elsi_with_pexsi", type='build')
        depends_on("flex@2.6.4", when="+elsi_with_pexsi", type='build')

        # Note that the 'cuda' variant is already implicitly declared by the parent class CudaPackage
        # variant("cuda", default=False, description="Enable CUDA for GPU acceleration")
        # conflicts("~cuda", when="+elsi_with_gpu", msg="CUDA is required to add GPU support to ELSI on the fly")
        # It seems to be enough to require cuda_arch != 'none' to enable +cuda in the spec
        conflicts("cuda_arch=none", when="+elsi_with_gpu", msg="CUDA arch is required by Siesta to compile ELSI on the fly")

        # Note that the advanced CPU kernels will not be honored if +elsi_with_gpu is in effect
        # This is an ELSI restriction
        variant(
            "elsi_elpa2_kernel",
            default="none",
            description="ELPA2 CPU Kernel",
            values=("none", conditional("AVX", "AVX2", "AVX512", when="+elsi_with_gpu")),
            multi=False,
        )

    depends_on('cmake@3.21.0:', type='build')

    # generator = 'Ninja'
    # depends_on('ninja', type='build')
    
    depends_on('lapack')
    depends_on('xmlf90@1.6.3:')
    depends_on('libpsml@2.0.1:')
    depends_on('libfdf@0.5.1:')
    depends_on('mpi', when='+mpi')
    depends_on('scalapack', when='+mpi')
    depends_on('netcdf-fortran', when='+netcdf')
    depends_on('libxc@4:5', when='+libxc')
    depends_on('libgridxc@2.0.2:')

    depends_on('elpa@2020.05:', when='+elpa')

    depends_on('fftw@3.3.0:', when='+fftw')
    depends_on('pexsi@2.0.0:', when='+pexsi')

    # Wannier90, dftd3, and flook dependencies are currently handled internally
    # by the Siesta build system

    # Conflicts between dependency variants and SIESTA variants

    # Grid single-point variant has to match for libgridxc and siesta
    conflicts('libgridxc~gridsp', when='+gridsp')
    conflicts('libgridxc+gridsp', when='-gridsp')

    # If SIESTA uses libxc, then libgridxc has to have libxc support (not vice versa)
    conflicts('libgridxc~libxc', when='+libxc')

    # If SIESTA does not use MPI, then libgridxc has to be compiled without MPI
    conflicts('libgridxc+mpi', when='~mpi')

    # If SIESTA uses MPI, then libgridxc has to be compiled with MPI
    conflicts('libgridxc~mpi', when='+mpi')

    # ELSI and ELPA need MPI
    conflicts('+elsi', when='~mpi')
    conflicts('+elpa', when='~mpi')

    # Toolchain conflicts
    conflicts("%nvhpc", when="+flook", msg="Flook cannot be compiled (yet) with nvhpc")

    # Patches for specific issues (could maybe go in the CMake logic?)
    patch("gnu-native-flag.patch",when="%gcc@12 platform=darwin target=m1")

    def cmake_args(self):

       args = [
            self.define_from_variant('SIESTA_WITH_MPI', 'mpi'),
            self.define_from_variant('SIESTA_WITH_OPENMP', 'openmp'),
            self.define_from_variant('SIESTA_WITH_LIBXC', 'libxc'),
            self.define_from_variant('SIESTA_WITH_NETCDF', 'netcdf'),
            self.define_from_variant('SIESTA_WITH_ELPA', 'elpa'),
            self.define_from_variant('SIESTA_WITH_PEXSI', 'pexsi'),
            self.define_from_variant('SIESTA_WITH_ELSI', 'elsi'),
            self.define_from_variant('SIESTA_WITH_FFTW', 'fftw'),
            self.define_from_variant('SIESTA_WITH_DFTD3', 'dftd3'),
            self.define_from_variant('SIESTA_WITH_FLOOK', 'flook'),
            self.define_from_variant('SIESTA_WITH_GRID_SP', 'gridsp'),
            self.define_from_variant('SIESTA_WITH_WANNIER90', 'wannier90'),
       ]

       # Handle cases in which Siesta cannot find Scalapack
       if '^scalapack' in self.spec:
            scalapack_libs = self.spec['scalapack'].libs
            scalapack_link_flags = scalapack_libs.ld_flags
            print("Scalapack libs: ", scalapack_libs)
            print("Scalapack link flags: ", scalapack_link_flags)
            # Pass the formatted string to CMake
            args.append(f"-DSCALAPACK_LIBRARY={scalapack_link_flags}")

       # Note that 'when' clauses do not seem to work well here

       if "@5.2:" in self.spec:

           if "+elsi" in self.spec:
               args += [
                   self.define_from_variant('ELSI_WITH_GPU', 'elsi_with_gpu'),
                   self.define_from_variant('SIESTA_WITH_ELSI_PEXSI', 'elsi_with_pexsi'),
               ]

               # This CMake variable is undocumented for now, and really inactive
               # It should be passed directly to the ELSI subproject
               # Note that GPU kernels are incompatible with non-generic kernels
               # in ELSI...
               if (
                       "elsi_elpa2_kernel" in self.spec.variants
                       and self.spec.variants["elsi_elpa2_kernel"].value != "none"
                  ):
                   kernel = self.spec.variants["elsi_elpa2_kernel"].value
                   args += ["-DELSI_ELPA2_KERNEL=" + kernel]

               if "+elsi_with_gpu" in self.spec:
                   cuda_arch_list = self.spec.variants["cuda_arch"].value
                   cuda_arch = cuda_arch_list[0]
                   if cuda_arch != "none":
                       args.append(f"-DCMAKE_CUDA_ARCHITECTURES={cuda_arch}")
       
       return args


    def get_commit_from_tag(self, tag):
        """ Get the commit hash associated with a Git tag. """
        import subprocess
        try:
            # Execute git command to get the commit hash for the tag
            commit = subprocess.check_output(
                ["git", "rev-list", "-n", "1", tag],
                cwd=self.stage.source_path,  # Change directory to source path
                text=True
            ).strip()
            return commit
        except subprocess.CalledProcessError:
            print(f"Could not retrieve commit for tag: {tag} (maybe git info is missing from cached source)")

    def cmake(self, spec, prefix):
        import os

        # Create the SIESTA.release file if it does not exist

        # Root directory path (where the top-level CMakeLists.txt is located)
        root_dir = self.stage.source_path

        print("Root directory path:", root_dir)

        version_details_path = os.path.join(root_dir, 'SIESTA.release')

        # Check if the SIESTA.release file already exists
        if not os.path.exists(version_details_path):

            version = str(spec.version)

            commit_from_git = self.get_commit_from_tag("HEAD")
            print("Commit from git:", commit_from_git)

            # Get the relevant data for the current version
            metadata = self._version_data.get(version, {})
            tag = metadata.get('tag', None)
            commit = metadata.get('commit', None)
            branch = metadata.get('branch', None)

            print("Version:", version)

            with open(version_details_path, 'w') as f:
                if tag:
                    print("Found tag: ",tag)
                    f.write(f"{version} (Spack) Tag: {tag}\n")
                elif commit:
                    print("Found commit: ",commit)
                    f.write(f"{version} (Spack) Commit: {commit}\n")
                elif branch:
                    print("Found branch: ",branch)
                    if commit_from_git is None:
                        f.write(f"{version} (Spack) Branch: {branch}\n")
                    else:
                        f.write(f"{version} (Spack) Branch: {branch} Commit: {commit_from_git}\n")
                else:
                    print("Cannot determine commit or tag info")
                    if commit_from_git is None:
                        f.write(f"{version} (Spack)\n")
                    else:
                        f.write(f"{version} (Spack) Commit: {commit_from_git}\n")

            # Now proceed with the default CMakePackage 'cmake' stage
            super(Siesta, self).cmake(spec, prefix)
