## Experimental SPACK packages

Make sure that you read the updated SIESTA building documentation in

       https://docs.siesta-project.org


(Please see the Environments directory for examples of Spack environments that would make it
easier to install Siesta with specific features.

The package recipes in the siesta-project-spack-repo are also
essential when working with environments.)


   After setting up spack and defining compilers, etc, a user can simply install
   a new repo with the 'siesta-project' namespace:

     spack repo add /path/to/siesta-project-spack-repo

   and issue commands such as:

     spack install xmlf90
     spack info siesta
     spack spec siesta +mpi +netcdf +libxc +elpa
     spack spec siesta +mpi +netcdf +libxc ^elpa@2024.05.001+cuda
     spack install siesta +mpi +netcdf +libxc ^elpa@2024.05.001+rocm
     spack install siesta -mpi build_type=Debug

   Note that the spack builtin repo *might have* other Siesta-related
   recipes prepared in the past by other members of the community.
   By making sure that the 'siesta-project' repo is listed first in the
   spack repository chain, those can be avoided. Check:

     spack repo list

Some third-party recipes (elpa, pexsi) might also be available in the
built-in repo, but the versions here should be used instead (until the
changes have been sent upstream and accepted).

These recipes might need some tweaking in specific systems, despite
spack's claims to portability. This is sometimes due to misconfiguration
(e.g. the early 'mpi' metapackage problem in Leonardo). Maintaining all those
tweaks in the Siesta repo is not workable, so we provide this kind
of information on specific systems in separate files.


