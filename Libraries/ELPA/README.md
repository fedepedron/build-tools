ELPA uses an auto-tools-based building system. Create a 'build'
subdirectory and execute the following script in it (This is
appropriate to the Intel development environment. Change paths
appropriately, and look for other recipes in this site).

```
#!/bin/sh

FC=mpiifort CC=mpiicc FCFLAGS="-O3 -ip" CFLAGS="-O3 -ip" \
LDFLAGS="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core" \
../configure --disable-legacy-interface --disable-sse-assembly --disable-sse \
             --enable-single-precision --prefix=$HOME/lib/prace/ifort-17.4/elpa-2020.05.001
```

ELPA-2020 is quite old, but it was a relatively well-functioning
version just after the implementation of GPU support. Then followed a
few other versions with a few problems. It now seems that the
2024.05.001 release (and its fixes, being applied in 'master' and
'master_pre_stage' in the ELPA repo) can be recommended.


