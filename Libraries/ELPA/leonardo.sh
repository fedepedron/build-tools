#
# Leonardo gcc
#
# module load  gcc/11.3.0
# module load  openblas/0.3.21--gcc--11.3.0              
# module load  openmpi/4.1.4--gcc--11.3.0-cuda-11.8   
# module load  cuda/11.8
#
#  Notes: '--enable-nvidia-gpu', and not '--enable-nvidia-sm80-gpu' is used,
#         to avoid kernel naming changes. Support for this needs to be extended.
#
#         The compiler flags are those suggested by the ELPA documentation.
#
#         To run Siesta, make sure that the ELPA lib directory is in the
#         LD_LIBRARY_PATH environment variable.
# -------
LAPACK_ROOT=${OPENBLAS_HOME}
SCALAPACK_ROOT=${NETLIB_SCALAPACK_HOME}

FC=mpifort CC=mpicc CXX=mpicxx CPP="gfortran -E" \
FCFLAGS="-O3 -march=native -mavx2 -mfma" \
CFLAGS="-O3 -march=native -mavx2 -mfma  -funsafe-loop-optimizations -funsafe-math-optimizations -ftree-vect-loop-version -ftree-vectorize" \
  LDFLAGS="-L${SCALAPACK_ROOT}/lib -lscalapack -L${LAPACK_ROOT}/lib -lopenblas -lpthread -lm -ldl -lstdc++" \
  ../configure \
             --enable-nvidia-gpu --with-NVIDIA-GPU-compute-capability=sm_80 \
             --enable-c-tests=no --prefix=$WORK/agarcia0/installs/gcc/11.3.0/elpa/2023.11.001-b593b7af
