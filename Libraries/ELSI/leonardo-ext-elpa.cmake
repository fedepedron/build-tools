SET(CMAKE_INSTALL_PREFIX "$ENV{BASE_DIR}/elsi-ext-elpa/v2.10.0--elpa-2023.05.001-21-g402039eb" CACHE STRING "Installation dir")

SET(CMAKE_Fortran_COMPILER "mpif90" CACHE STRING "MPI Fortran compiler")
SET(CMAKE_C_COMPILER "mpicc" CACHE STRING "MPI C compiler")
SET(CMAKE_CXX_COMPILER "mpicxx" CACHE STRING "MPI C++ compiler")

SET(CMAKE_Fortran_FLAGS "-O2 -g -fbacktrace -fdump-core -fallow-argument-mismatch" CACHE STRING "Fortran flags")
SET(CMAKE_C_FLAGS "-O2 -g -std=c99" CACHE STRING "C flags")
SET(CMAKE_CXX_FLAGS "-O2 -g -std=c++11" CACHE STRING "C++ flags")
SET(CMAKE_CUDA_FLAGS "-O3 -arch=sm_80 -std=c++11" CACHE STRING "CUDA flags")
SET(CMAKE_CUDA_ARCHITECTURES "80" CACHE STRING "CUDA architectures")
# Workaround: specify -std=c++11 in CMAKE_CUDA_FLAGS to avoid __ieee128 gcc/cuda bug
SET(USE_EXTERNAL_ELPA ON CACHE BOOL "Use external ELPA")
SET(USE_GPU_CUDA ON CACHE BOOL "Use CUDA-based GPU acceleration in ELPA")
SET(ENABLE_PEXSI OFF CACHE BOOL "Enable PEXSI")
SET(ENABLE_TESTS ON CACHE BOOL "Enable tests")
#SET(ADD_UNDERSCORE OFF CACHE BOOL "Do not suffix C functions with an underscore")

SET(INC_PATHS "$ENV{ELPA_ROOT}/include/elpa-2023.05.001/modules" CACHE STRING "External library include paths")
SET(LIB_PATHS "$ENV{ELPA_ROOT}/lib;$ENV{OPENBLAS_HOME}/lib;$ENV{NETLIB_SCALAPACK_HOME}/lib;$ENV{CUDA_HOME}/lib64" CACHE STRING "External library paths")

SET(LIBS "elpa;scalapack;openblas;cublas;cudart" CACHE STRING "External libraries")
