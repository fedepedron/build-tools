# Notes on compilation of ELSI for Siesta

##  Peculiarities of the CMake system in ELSI

- Instead of using `find_package` to search for dependencies, one has
to put in a toolchain file the needed libraries, their paths, and the
paths to the relevant module files in `LIBS`, `LIB_PATHS`, and
`INC_PATHS`.  Internally, the CMake system uses `find_library` with
the path hints to build the dependencies. Library names and paths are
added automatically to a list which serves to create the .pc file. In
contrast, only internal CMake targets are installed, and their
discovery is not encoded in the package file.  Hence, the pkg-config
route is the most dependable to obtain the linking incantation.

- ELSI depends on "toolchain files" to declare the compiler names and
  options, and any special flags. This complicates a bit the control
  by higher-level tools such as spack (e.g. to add the
  `-fallow-argument-mismatch` for gfortran >=10.0).

- Instead of establishing an explicit dependency on MPI, ELSI
  uses the MPI-enabled compilers (mpifort, etc). 

For these reasons, in versions of Siesta beyond 5.2.0-alpha, ELSI
is now compiled on-the-fly using "sub-project" mode with a patched
ELSI distribution. Dependencies are automatically processed.

## Code issues

- The PEXSI subsystem is somewhere post 1.2.0 and before 2.0.0 in
  relation to the original PEXSI library. It is beyond 1.2.0 in that
  it re-enables the handling of the entropy.

- ELSI-PEXSI with method=3 does not work when using an external 2.1
  PEXSI. This arguably is due to a bug in 2.1, which is still
  pre-release.

- Builtin-ELPA used to be offered in several versions:

 * 2020.05
 * 2021.11
 * 2023.05
 * 2024.03

All of them are "preprocessed" versions of the original ELPA code.

The lack of standard affinity calls in MacOS means that newer versions
of ELPA do not compile on that platform.

There were some issues with GPU operation in the 21 and 23 versions
that seemed to be solved with the 2024.03 version. However, the pre-processing
of the ELPA code introduces some subleties that have led the ELSI
developers to remove support for built-in versions of ELPA beyond 2020.05. Using
an external ELPA library is recommended.






