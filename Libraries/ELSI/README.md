The new ELSI interface in Siesta offers unified access to various
solvers, with a number of performance-enhancement options.

(Please read the ELSI manual for a full description of the options)

Using an external ELPA library enables the use of the single-precision
option, the auto-tune feature, as well as dynamic selection of ELPA2
kernels.  GPU support is available both in ELPA itself and in the
built-in ELPA code in the ELSI library.

For instructions on compilation of an external ELPA library, please see
the appropriate section in this site.

* Compilation of the ELSI library

This is now (for versions of Siesta beyond 5.2.0-alpha) done
automatically on-the-fly during the compilation of Siesta, if the
-DSIESTA_WITH_ELSI=ON is in effect (it is by default). See the
INSTALL.md file in Siesta.

* Building Siesta with ELSI and an external ELPA library (recommended)

At the CMake configuration step just define

  -DSIESTA_WITH_ELPA=ON 

and make sure that you include the $ELPA_ROOT directory in
CMAKE_PREFIX_PATH. (Using an external ELPA library is the
default behavior if it is found by Siesta's build system.)

* Using the ELSI solvers for increased performance in large HPC systems.

A first noteworthy feature common in principle to all solvers is that
the SIESTA-ELSI interface is fully parallelized over k-points and
spins (no support yet for non-collinear spin). This means that these
calculations can use two extra levels of parallelization (in addition
to the standard one of parallelization over orbitals and real-space
grid).

- ELPA solver in single-precision mode

When compiled as an external library (as described above), the ELPA
solver can be used in "single-precision" mode.  This can lead to
substantial savings in CPU time. To enable this mode, include in the
fdf file:

```
  solution-method ELSI
  elsi-solver ELPA
  elsi-elpa-n-single-precision N
  dm-normalization-tolerance 0.001   # (To avoid too stringent an intermediate check)
```

where N is the number of scf steps to be run in single-precision.

It has been found empirically that most scf steps can be run in
single-precision, and just the final one or two in double precision,
to reproduce the results of a full-double run.

- ELPA solver with GPU support

when compiled appropriately (with CUDA/ROCm options in addition to those
described above --see the ELSI (CUDA only) or the ELPA documentation), the ELPA solver
has support for GPU offloading.  To enable this feature, use the flag:

```
  elsi-elpa-gpu 1
```

GPU support is available both in the built-in ELPA in ELSI and in
an externally-compiled ELPA library.

- PEXSI solver

This solver has reduced scaling (at most O(N^2) for dense systems, and
O(N) for quasi-one-dimensional systems) and *two* extra levels of
parallelization: over poles, and over trial points for
chemical-potential interpolation.  It can be used for large systems
with very high numbers of processors.

```
   solution-method elsi
   elsi-solver pexsi
   elsi-pexsi-tasks-per-pole  Npp     
   elsi-number-of-mu-points Nmu       (default 2)
   elsi-number-of-poles     Npoles    (default 30 for the AAA method) 
```

The total number of processors used must be a multiple of Npp*Nmu*Nkpoints*Nspins,
and must also include an extra factor which divides the number of poles, so that
these can be processed in batches.

For complete parallelization over poles, number of processors =  Npp*Nmu*Nkpoints*Nspins*Npoles

- NTPoly solver
(Built-in in ELSI)

It uses density-matrix purification algorithms to achieve linear
scaling for suitably gapped systems.  See the manual for the tolerance
options involved.

- EigenExa solver  (not interfaced yet from within Siesta. Merge requests welcome)

It needs an externally compiled EigenExa library from
```
    https://www.r-ccs.riken.jp/labs/lpnctrt/en/projects/eigenexa/
```
It uses a novel algorithm for diagonalization of penta-diagonal
matrices, with favorable scaling. It can compute a full set of
eigenvectors or none, which reduces somewhat its usefulness.








