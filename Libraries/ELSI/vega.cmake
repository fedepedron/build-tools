### GCC ###

SET(CMAKE_Fortran_COMPILER "mpifort" CACHE STRING "MPI Fortran compiler")
SET(CMAKE_C_COMPILER "mpicc" CACHE STRING "MPI C compiler")
SET(CMAKE_CXX_COMPILER "mpicxx" CACHE STRING "MPI C++ compiler")

SET(CMAKE_Fortran_FLAGS "-fallow-argument-mismatch -O3" CACHE STRING "Fortran flags")
SET(CMAKE_C_FLAGS "-O3 -std=c99" CACHE STRING "C flags")
SET(CMAKE_CXX_FLAGS "-O3 -std=c++11" CACHE STRING "C++ flags")
#SET(CMAKE_CUDA_FLAGS "-O3 -arch=sm_80" CACHE STRING "CUDA flags")
SET(CMAKE_CUDA_ARCHITECTURES "80" CACHE STRING "CUDA architectures")

SET(USE_GPU_CUDA ON CACHE BOOL "Use CUDA-based GPU acceleration in ELPA")
SET(ENABLE_PEXSI ON CACHE BOOL "Enable PEXSI")
SET(ENABLE_TESTS ON CACHE BOOL "Enable Fortran tests")
SET(ENABLE_C_TESTS ON CACHE BOOL "Enable C tests")

SET(LIB_PATHS "$ENV{EBROOTSCALAPACK}/lib $ENV{EBROOTOPENBLAS}/lib  $ENV{EBROOTCUDA}/lib64" CACHE STRING "External library paths")
SET(LIBS "scalapack openblas cublas cudart" CACHE STRING "External libraries")
