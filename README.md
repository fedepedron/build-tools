# Build Tools

This repo contains scripts, Spack recipes, CMake toolchains, and other material to
help build Siesta, its related libraries, and any other code that
might be useful in the Siesta ecosystem.

The information can be arranged by topic (i.e. package) and/or by
system (e.g. EuroHPC supercomputers). The final structure is still to
be refined. In the meantime information can be added here by those
people with direct access, or the repo can be forked and merge requests
prepared.
