## Notes on development versions of Siesta for deployers.

### Version with ELSI interface (with updated GPU support)

You need the 'dev' version of Siesta from the Gitlab site

  https://gitlab.com/siesta-project/siesta.git

GPU support is picked up by Siesta if the ELSI library has it. 

### Version 5.0.X

These do not contain the ELSI interface, but can run on GPUs (with a
slightly outdated set of features) with the native ELPA interface. You
need ELPA-2024.05.001 and the option

```
 -DSIESTA_WITH_ELPA=ON
```

in the CMake invocation. Make sure to pick up a recent version along the
rel-5.0 branch.


The toolchain files in this directory might become outdated as the details of
the software stack on different computers change.

